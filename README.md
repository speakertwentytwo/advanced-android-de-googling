# S22's Advanced De-Googling guide for Android devices


**Before we jump in:** If your phone does not support a privacy friendly ROM or installing custom software at all, consult my [Digital Privacy Guide](https://gitlab.com/stwentytwo/digital-privacy-guide) and consider the following alternatives:
- Use a phone [supported by](https://calyxos.org/docs/guide/device-support) [CalyxOS](https://calyxos.org)
- Semi-degoogle your Android phone
- Use an iPhone

## Introduction

**Android phones are not privacy friendly by default, but you often have the ability to install different versions of Android - known as a "custom ROM."** Thanks to many open-source projects, you can have a phone with exactly the software you want - fully degoogled, or any balance that preserves privacy according to your threat model.

MicroG is an open-source implementation of google's Cloud Messaging, Location, and other services. This allows common proprietary apps you 'need' to function correctly, and send notifications. If you want this, (you probably do) ensure your ROM lists support for MicroG or "signature spoofing" in its feature list.

Check [Plexus](https://plexus.techlore.tech) to see if your apps function with MicroG or a fully degoogled OS. 


## Installing and choosing a custom ROM

This is arguably the trickiest part. You must be able to unlock the bootloader on your device to install a different OS. If your phone is a recent Samsung with a Snapdragon processor for example, it is nearly impossible. With some devices, you only have to toggle a setting and run an ADB command. With some, you have to obtain a code or even create an account through the manufacturer's portal. It differs from one device to another. The same applies to the installation process - sometimes you must install a custom recovery (a low level on-device tool to modify the software), sometimes not.

1. With the packaging or the about phone pages in settings, determine your exact device variant and its codename. For example, the international Samsung S4 is codenamed jfltexx, model GT-I9505, and the Canadian version is jfltecan, SGH-1337M. These names are used in forums and could mean the difference between a compatible and incompatible ROM. Codenames are something you will have to look up as they are not listed on the device.
2. Search for ROMs for your device! [XDA](https://forum.xda-developers.com) is still the hub for modding - just type your device in the search bar, head over to the pinned ROMs thread, and browse around.
3. Pick one that you like! For security reasons, I recommend using the newest available version of Android. If you want to use MicroG as per above, make sure it's supported. If one ROM has an easier installation method for the same device, that might inform your decision.
4. Follow their installation process and any linked guides. Again this differs from device to device, so I can't help you here.

## Post install setup

Congrats! It feels like you've freed your phone from the clutches of its corporate overlords. But there's a little more to consider before you can really *use* this phone.

- LineageOS and other ROMs often have a few small bits of google remaining. Consult [this video](https://yewtu.be/E1U5qoiR1fM) by Mental Outlaw to fix that. 
- Unless you use something like CalyxOS or [/e/OS](https://e.foundation) that has MicroG included, you'll have to install it and make sure everything is working correctly. [This video](https://yewtu.be/fhfWJmFMVQw) is one of the best guides to doing that.
- Obviously, on a de-googled phone, never sign into a google account. Your browser can be a safe exception to this if you need to access some google services for now.
- Use privacy-respecting alternatives to common apps, quality open source apps from F-droid, and enjoy!

## About this guide

There's a lack of guides specifically on installing custom ROMs for this purpose - I wanted to keep things simple and provide all the information you might need in one place. This is also linked in my [Digital Privacy Guide](https://gitlab.com/stwentytwo/digital-privacy-guide) to keep advanced steps like this out of the guide.

At some point, I may include information covered in the two linked videos as part of the guide. For now, I trust those resources and am more then happy to lean on them. Thanks for reading!

